-- [SECTION] Advance selects

-- Exclude records or NOT operator
SELECT * FROM songs WHERE id != 3;

-- GREATER THAN OR EQUAL;
SELECT * FROM songs WHERE id > 3;

SELECT * FROM songs WHERE id >= 3;

-- LESS THAN OR EQUAL;
SELECT * FROM songs WHERE id < 11;

SELECT * FROM songs WHERE id <= 11;

-- IN operator (songs id with 1, 3, 11)
SELECT * FROM songs WHERE id IN (1, 3, 11);

SELECT * FROM songs WHERE genre IN ("Pop", "KPOP");

--Partial Matches.
SELECT * FROM songs WHERE song_name LIKE "%a";


SELECT * FROM songs WHERE song_name LIKE "a%";


SELECT * FROM songs WHERE song_name LIKE "%a%";

SELECT * FROM songs WHERE song_name LIKE "%s_y%";


SELECT * FROM songs WHERE song_name LIKE "%1";

-- SORTING records 
SELECT * FROM songs ORDER BY song_name ASC; 

SELECT * FROM songs ORDER BY song_name DESC; 

-- Getting distinct record 
SELECT DISTINCT genre FROM songs;

SELECT COUNT(genre) FROM songs WHERE genre = "Pop"; 


-- [INNER JOIN] 
SELECT albums.album_title, song.song_name FROM albums JOIN songs ON albums.id = songs.album_id; 

-- LEFT JOIN 
SELECT * FROM albums LEFT JOIN songs ON albums.id = songs.album_id;

-- JOINING MULTIPLE tables
SELECT * FROM artists JOIN albums ON artist.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- RIGHT JOIN
SELECT * FROM albums RIGHT JOIN songs ON albums.id = songs.album_id;

-- OUTER JOIN
SELECT * FROM albums LEFT OUTER JOIN songs ON albums.id = songs.album_id; 
	UNION 
	SELECT * FROM albums RIGHT OUTER JOIN songs ON albums.id = songs.album_id;
















